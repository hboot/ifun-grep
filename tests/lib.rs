use assert_cmd::prelude::*;
use assert_fs::prelude::*;
use ifun_grep::{find, find_insensitive};
use predicates::prelude::*;
use std::{error::Error, process::Command};

#[test]
fn case_sensitive() {
    let search = "rust";
    let content = "\
nice. rust
I'm hboot.
hello world.
Rust
";

    assert_eq!(vec!["nice. rust"], find(search, content));
}

#[test]
fn case_insensitive() {
    let search = "rust";
    let content = "\
nice. rust
I'm hboot.
hello world.
Rust
";

    assert_eq!(
        vec!["nice. rust", "Rust"],
        find_insensitive(search, content)
    );
}

#[test]
fn file_doesnt_exist() -> Result<(), Box<dyn Error>> {
    let mut cmd = Command::cargo_bin("ifun-grep")?;

    cmd.arg("-s let").arg("-f 1.txt");
    cmd.assert()
        .failure()
        .stderr(predicate::str::contains("could not read file"));

    Ok(())
}

#[test]
fn file_content_exist() -> Result<(), Box<dyn Error>> {
    let file = assert_fs::NamedTempFile::new("1.txt")?;
    file.write_str("hello world \n Rust-web \n good luck for you!")?;

    let mut cmd = Command::cargo_bin("ifun-grep")?;

    cmd.arg("-s good").arg("-f").arg(file.path());
    cmd.assert()
        .success()
        .stderr(predicate::str::contains("good luck for you!"));

    Ok(())
}
