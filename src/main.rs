use ansi_term::Colour::{Green, Yellow};
use ansi_term::Style;
use clap::Parser;
use env_logger::Env;
use log;
use std::process;

use ifun_grep::{run, Config};

fn main() {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    let config = Config::parse();

    log::info!(
        "will search {} in {}",
        Green.bold().paint(&config.search),
        Style::new().fg(Yellow).paint(&config.file_path)
    );
    if let Err(e) = run(config) {
        log::error!("something error:{:?}", e);
        process::exit(1);
    }
}
